from math import *

def isPrime(num):
	if num == "exit":
		exit()
	num = int(num)
	for i in range(1,int(sqrt(num))+1):
		if num%i == 0 and i != 1 and i != num:
			return False
	return True

def euler(num):
	primes = []
	for i in range(1,num):
		if gcd(i,num)[0] == 1:
			primes.append(i)
	return primes, len(primes)

def gcd(a , b):
	x,y, u,v = 0,1, 1,0
	while a != 0:
		q,r = b//a,b%a
		m,n = x-u*q,y-v*q
		b,a, x,y, u,v = a,r, u,v, m,n
	return b, x, y

def primeFactors(num):
	curr = num
	factorization = []
	if isPrime(num): return [num]
	nextPrime = 2
	while not isPrime(curr):
		while not isPrime(nextPrime):
			nextPrime += 1
		if curr%nextPrime == 0:
			curr = curr//nextPrime
			factorization.append(nextPrime)
		else:
			nextPrime += 1
	factorization.append(curr)
	return factorization

def modinv(a, m):
	g, x, y = gcd(a, m) 
	if g != 1:
		return None
	else:
		return x % m

def chinese():
	num = int(input("How many equations are in the system? ")) 
	alist,mlist,a,m = [],[],1,1
	for i in range(1, num + 1):
		b = input("a" + str(i) + ",m" + str(i) + ": ").split(",")
		print(b)
		j,k = eval(b[0]),eval(b[1])
		a = j*a
		m = k*m
		alist.append(j)
		mlist.append(k)
	mWorkList = [m/i for i in mlist]
	invList = [modinv(mWorkList[i], mlist[i]) for i in range(len(mlist))]
	total = 0
	for i in range(len(mlist)):
		total += alist[i] * invList[i] * mWorkList[i]
	print("a List =", alist)
	print("m List =", mlist)
	print("M =", m)
	print("M List =", mWorkList)
	print("Y List =", invList)
	return int(total%m)

def series(term):
	for i in range(6):
		term = (9*term + 5)%4
		print (term)

def decode(astring):
	answer = ""
	letters = {1:'a',2:'b',3:'c',4:'d',5:'e',6:'f',7:'g',8:'h',9:'i',10:'j',11:'k',12:'l',13:'m',14:'n',15:'o',16:'p',17:'q',18:'r',19:'s',20:'t',21:'u',22:'v',23:'w',24:'x',25:'y',26:'z'}
	temp = []
	for c in astring:
		temp.append(((ord(c)-97)*4+6)%26)
	for i in temp:
		answer += chr(97+i)
	return answer

def primesBetween(x,n):
	y,primes = 1,[]
	while y != (n+1)/2:
		if isPrime(y):
			primes.append(y)
			print(y)
		y += 1
	return primes

def congruency(a,b,m):
	i = 0
	while ((a*i)-b)%m != 0:
		i+=1
	return i

def affineE(a,b,msg):
	encrypt,n = [],26
	print(n)
	for c in msg:
		encrypt.append((a*(ord(c)-97)+b)%n)
	return encrypt

def affineD(a,b,msg):
	decrypt,n = [],26
	letters = {1:'a',2:'b',3:'c',4:'d',5:'e',6:'f',7:'g',8:'h',9:'i',10:'j',11:'k',12:'l',13:'m',14:'n',15:'o',16:'p',17:'q',18:'r',19:'s',20:'t',21:'u',22:'v',23:'w',24:'x',25:'y',26:'z'}	
	for c in msg:
		decrypt.append(letters[((modinv(a,n)*(c-b))%n)])
	return decrypt

def probPrime(p,m):
	for a in range(1, m):
		if modinv(a**(p-1),p) != 1:
			return False
	return True

def c(n,r):
	return factorial(abs(n))/(factorial(abs(n-r))*factorial(abs(r)))

def p(n,r):
	return factorial(abs(n))/factorial(abs(n-r))

def exactHeads(maximum,total,unique):
	answer = 0
	for i in range(1,total+1):
		if i != 10:
			answer += c(total,i)
	print(answer)
	return (unique**total)-answer

def numMinFor(minNo,spots):
	return (spots*minNo)+1

def minNumIn(total, spots):
	return ceil(total/spots)